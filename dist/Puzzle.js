var Puzzle = (function () {
    function Puzzle(size) {
        this.size = size;
        this.tiles = [];
        this.buildTiles();
    }
    Puzzle.prototype.buildTiles = function () {
        var possibleNumbers = [];
        for (var i_1 = 1; i_1 < this.size; i_1++) {
            possibleNumbers.push(i_1);
        }
        ArrayUtility.shuffle(possibleNumbers);
        var numberOfRowsAndColumns = Math.sqrt(this.size);
        var i = 1;
        for (var y = 1; y <= numberOfRowsAndColumns; y++) {
            for (var x = 1; x <= numberOfRowsAndColumns; x++) {
                var number = Math.floor(Math.random() * possibleNumbers.length);
                var roll = possibleNumbers.splice(number, 1);
                this.tiles[i] = new Tile(x, y, roll[0]);
                i++;
            }
        }
        this.tiles.splice(0, 1);
    };
    Puzzle.prototype.isSolved = function () {
        for (var i = 1; i < this.tiles.length; i++) {
            if (this.tiles[i].value > this.tiles[i + 1].value) {
                return false;
            }
        }
        return true;
    };
    Puzzle.prototype.getStatus = function () {
        console.log(this.isSolved() ? "Puzzle is solved." : "Puzzle is not solved.");
        var tiles = this.tiles.slice();
        var rows = [];
        var numberOfRows = Math.sqrt(this.size);
        while (tiles.length > 0) {
            rows.push(tiles.splice(0, numberOfRows));
        }
        rows.forEach(function (row, index) {
            console.log("Row " + (index + 1) + ": " + row.join(" | "));
        });
    };
    Puzzle.prototype.moveTile = function (source, target) {
        if (!this.isMoveable(source, target)) {
            throw "IllegalMoveException";
        }
        var sourceTileIndex = this.tiles.findIndex(function (tile) {
            return tile.value === source.value;
        });
        var targetTileIndex = this.tiles.findIndex(function (tile) {
            return tile.value === target.value;
        });
        var temp = this.tiles[sourceTileIndex];
        this.tiles[sourceTileIndex] = target;
        this.tiles[targetTileIndex] = temp;
    };
    Puzzle.prototype.isMoveable = function (source, target) {
        if (typeof source.value !== "undefined" && typeof target.value !== "undefined") {
            return false;
        }
        var moveIsPossible = false;
        var possibleMoves = this.tiles.filter(function (tile) {
            if (tile.x === source.x) {
                return tile.y === source.y - 1 || tile.y === source.y + 1;
            }
            else {
                return tile.x === source.x - 1 || tile.x === source.x + 1;
            }
        });
        possibleMoves.forEach(function (tile) {
            if (tile.value === target.value) {
                moveIsPossible = true;
            }
        });
        return moveIsPossible;
    };
    return Puzzle;
}());
