var Tile = (function () {
    function Tile(x, y, value) {
        this.x = x;
        this.y = y;
        this.value = value;
    }
    Tile.prototype.toString = function () {
        if (this.value) {
            return this.value.toString();
        }
        return " ";
    };
    return Tile;
}());
