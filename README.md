## Introduction

I wrote the app in TypeScript. I used Jasmine for writing unit tests and included the standalone version in this repository. The unit tests can be found in the `specs` directory. 

## How to build it
Install the type definitions with `npm install`. 

To build the app, you'll need the TypeScript compiler. To build the app, run `tsc`. This will create files in the `dist` folder. 

In case you don't have npm or the TypeScript compiler installed, I included the compiled files in this repository in the `dist` folder. 

## How to run it
Open `index.html` in your browser. 

### Features
* Basic logic of the game
* It's possible to move tiles
* Indicate whether puzzle is solved
* Get status of the puzzle

## How to run tests
Open `SpecRunner.html` in your browser.

Thank you for reviewing my app and have a nice day. 