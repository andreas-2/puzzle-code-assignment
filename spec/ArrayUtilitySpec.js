describe("ArrayUtility", () => {
    it("Shuffles correctly", () => {
        let array = [1, 2, 3, 4, 5, 6, 7, 8, 900, 1000, 1005];
        ArrayUtility.shuffle(array);
        expect(array[0]).not.toBe(1);
    });
});