describe("Initialization", () => {
    it("Puzzle initializes correctly", () => {
        let puzzle = new Puzzle(4 * 4);
        expect(puzzle.tiles.length).toEqual(16);

        puzzle = new Puzzle(3 * 3);
        expect(puzzle.tiles.length).toEqual(9);

        // todo Test for duplicate values
    });

    it("Puzzle is shuffled", () => {
        let puzzle = new Puzzle(4 * 4);
        expect(puzzle.isSolved()).toBe(false);
    });
});

describe("Movement of tiles", () => {
    beforeEach(() => {
        puzzle = new Puzzle(4 * 4);
    });

    it("Illegal move because distance is too big", () => {
        expect(
            puzzle.isMoveable(
                puzzle.tiles[0],
                puzzle.tiles[7]
            )
        ).toBe(false)
    });

    it("Illegal move because of two tiles can't switch", () => {
        expect(
            puzzle.isMoveable(
                puzzle.tiles[0],
                puzzle.tiles[1]
            )
        ).toBe(false)
    });

    it("Illegal move throws an exception", () => {
        expect(
            () => {
                puzzle.moveTile(
                    puzzle.tiles[0],
                    puzzle.tiles[1]
                )
            }
        ).toThrow("IllegalMoveException")
    });

    it("Legal move", () => {
        expect(
            puzzle.isMoveable(
                puzzle.tiles[14],
                puzzle.tiles[15]
            )
        ).toBe(true)
    });

    it("Moves a tile", () => {
        const source = puzzle.tiles[14];
        puzzle.moveTile(source, puzzle.tiles[15]);

        expect(puzzle.tiles[15].value).toBe(source.value)
    });
});