class Tile 
{
    x: number;
    y: number;
    value: number;

    constructor(x: number, y: number, value?: number) {
        this.x = x;
        this.y = y;
        this.value = value;
    }

    toString(): string {
        if (this.value) {
            return this.value.toString();
        }
        return " ";
    }
}