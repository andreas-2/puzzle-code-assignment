class Puzzle 
{
    size: number;
    tiles: Tile[];

    constructor (size: number) {
        this.size = size;
        this.tiles = [];

        this.buildTiles();
    }

    buildTiles() {
        let possibleNumbers: number[] = [];

        for (let i: number = 1; i < this.size; i++) {
            possibleNumbers.push(i);
        }

        ArrayUtility.shuffle(possibleNumbers);

        const numberOfRowsAndColumns = Math.sqrt(this.size);
        let i = 1;
        
        for (let y: number = 1; y <= numberOfRowsAndColumns; y++) {
            for (let x: number = 1; x <= numberOfRowsAndColumns; x++) {
                let number = Math.floor(Math.random() * possibleNumbers.length);
                var roll = possibleNumbers.splice(number, 1);
                this.tiles[i] = new Tile(x, y, roll[0]);
                i++;
            }
        }
        
        // The first tile must not be undefined
        this.tiles.splice(0, 1);
    }

    isSolved() {
        for (let i = 1; i < this.tiles.length; i++) {
            if (this.tiles[i].value > this.tiles[i + 1].value) {
                return false;
            }
        }

        return true;
    }

    getStatus() {        
        console.log(this.isSolved() ? "Puzzle is solved." : "Puzzle is not solved.");

        let tiles = this.tiles.slice();
        let rows = [];
        const numberOfRows = Math.sqrt(this.size);

        while (tiles.length > 0) {
            rows.push(tiles.splice(0, numberOfRows));
        }

        rows.forEach((row, index) => {
            console.log(`Row ${index + 1}: ` + row.join(" | "));
        })
    }

    moveTile(source: Tile, target: Tile) {
        if (!this.isMoveable(source, target)) {
            throw "IllegalMoveException";
        }
        
        const sourceTileIndex = this.tiles.findIndex((tile) => {
            return tile.value === source.value;
        });

        const targetTileIndex = this.tiles.findIndex((tile) => {
            return tile.value === target.value;
        });

        const temp = this.tiles[sourceTileIndex];

        this.tiles[sourceTileIndex] = target;
        this.tiles[targetTileIndex] = temp;
    }

    isMoveable(source: Tile, target: Tile): boolean {
        if (typeof source.value !== "undefined" && typeof target.value !== "undefined") {
            return false;
        }

        let moveIsPossible = false;

        const possibleMoves = this.tiles.filter((tile) => {
            if (tile.x === source.x) {
                return tile.y === source.y - 1 || tile.y === source.y + 1;
            } else {
                return tile.x === source.x - 1 || tile.x === source.x + 1;
            }
        });

        possibleMoves.forEach((tile) => {
            if (tile.value === target.value) {
                moveIsPossible = true;
            }
        });

        return moveIsPossible;
    }
}